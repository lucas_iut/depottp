package tp3;

public class Banque {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Compte c1;
		c1 = new Compte(0);
		
		c1.afficherSolde();
		
		VerserPleinDArgent l, m;
		l= new VerserPleinDArgent(c1);
		m= new VerserPleinDArgent(c1);
		Thread thread1 = new Thread(l);
		Thread thread2 = new Thread(m);
		thread1.start();
		thread2.start();
		c1.afficherSolde();
	}

}
