package tp3;
import java.awt.Container;
import java.awt.GridLayout;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

public class UneApplicationGraphique extends JFrame{
	private Container panneau;
	private JButton monBouton;
	private JLabel monLabel;

	public UneApplicationGraphique(){
		//Cr�ation de la fen�tre
		super("Mon Application Graphique");
		setSize(35, 150);
		setLocation(20,20);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		//R�cup�ration du container
		panneau = getContentPane();
		panneau.setLayout(new GridLayout(3,1));

		//Cr�ation des objets � utiliser
		monLabel = new JLabel("Mon label");
		monBouton = new JButton("Mon Bouton");

		//Ajout au panneau
		panneau.add(monLabel);
		panneau.add(monBouton);

		setVisible(true);
	}

	public static void main(String[] args) {
		UneApplicationGraphique n = new UneApplicationGraphique();
	}
}
