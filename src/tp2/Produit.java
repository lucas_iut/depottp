package tp2;

public abstract class Produit {

	public String nom;
	public float prixAchat;
	public float prixVente;
	public float quantite;
	public float caisse;

	Produit(String n, float pA, float pV, float qt){
		nom = n;
		prixAchat =pA;
		prixVente =pV;
		quantite = qt;
		caisse = 0;
	}

	public void combienCaisse() {
		System.out.println("Il y a "+caisse+" balle");
	}

}
