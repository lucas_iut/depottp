package tp2;

public class TestMagasin {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Appareils t;
		FruitsAlaPiece a;
		FruitsEnVrac b;

		t = new Appareils(0,"TÚlÚvision",10,20,5);
		a = new FruitsAlaPiece("Annanas","Reunion",1,2,1);
		b = new FruitsEnVrac("Bannane","Reunion",1,2,5);
		
		t.sePresenter();
		a.sePresenter();
		b.sePresenter();

		t.augmenterStock(1);
		t.sePresenter();

		a.augmenterStock(2);
		a.sePresenter();

		b.sePresenter();
		b.augmenterStock(1500);
		b.sePresenter();
			
		//EXO2
		t.vendreProduit(3);
		t.sePresenter();
		
		b.vendreProduit(500);
		b.sePresenter();
		
		a.vendreProduit(2);
		a.sePresenter();
		
		b.combienCaisse();
		t.combienCaisse();
		
	}

}
