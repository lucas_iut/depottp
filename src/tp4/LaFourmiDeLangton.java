package tp4;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.GridLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class LaFourmiDeLangton extends JFrame{
	private Container panneau;
	private JButton monBouton;
	private JLabel monLabel;
	private JTextField plateau[][];
	int dimension =10;

	public LaFourmiDeLangton(){
		//Cr�ation de la fen�tre
		super("La Fourmi de Langton");
		setSize(500, 500);
		setLocation(300,200);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		//R�cup�ration du container
		panneau = getContentPane();
		panneau.setLayout(new BorderLayout());
		//Cr�ation des objets � utiliser
		monLabel = new JLabel("Nombre de tours:");
		monBouton = new JButton("Next");
		//Ajout au panneau
		panneau.add(monLabel,BorderLayout.NORTH);
		panneau.add(creerPlateau(),BorderLayout.CENTER);
		panneau.add(monBouton,BorderLayout.SOUTH);
		
		setVisible(true);
	}
	
	public static void main(String[] args) {
		LaFourmiDeLangton n = new LaFourmiDeLangton();
	}
	
	public JPanel creerPlateau() {
		JPanel p = new JPanel();
		p.setLayout(new GridLayout(dimension,dimension));
		plateau = new JTextField[dimension][dimension];
		for(int i=0;i<dimension;i++) {
			for(int j=0;j<dimension;j++) {
			JTextField s = new JTextField();
			p.add(s);
			
			plateau[i][j]=s;
			
			plateau[i][j].setBackground(Color.BLACK);
			}	
		}
		
		return p;	
	}
	
	public void initFourmi() {
		Fourmi f1 = new Fourmi(dimension, dimension);
		f1.x=dimension /2;
		f1.y=dimension /2;
		
		
		
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
