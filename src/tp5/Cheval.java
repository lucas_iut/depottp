package tp5;

public class Cheval extends Thread implements CoureurHippique{

	int id;
	int longueur;
	int position;
	JugeDeCourse juge;
	
	public Cheval(int i,int l,JugeDeCourse j) {
		id=i;
		longueur=l;
		juge=j;

	}

	public void run() {
		//depart
		position =0;
		//course
		while(position<=longueur){
			System.out.println("Cheval "+id+" distance = "+position);
			position=position+1;
			try {
				sleep(50);
				
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		
		} 
		//fin de course
	 juge.passeLaLigneDArrivee(id);
		
		
	}

	@Override
	public int distanceParcourue() {
		// TODO Auto-generated method stub
		return position;
	}
		
	
}
