package tp6;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.*;


public class ChatClient extends JFrame implements ActionListener {

	/*___________ Attributs ______________*/
	private JTextField messageAEnvoyer;		//Zone de saisie du message
	private JButton b_Envoyer;				//Le bouton d'envoi du message
	private Socket maSocket;				//Socket du programme client
	private int numeroPort = 8888;			//Port 
	private String adresseServeur = "localhost";//Adresse du serveur
	private PrintWriter writerClient;		//Objet permettant l'�criture de message sur le socket
	private BufferedReader readerClient;

	/*___________ Constructeur ______________*/
	public ChatClient(){
		//D�finition de la fen�tre
		super("Client - Panneau d'affichage");
		setSize(250, 120);
		setLocation(300,300);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		//Cr�ation des composants graphiques
		messageAEnvoyer = new JTextField(20);
		b_Envoyer = new JButton("Envoyer");
		b_Envoyer.addActionListener(this);

		//Disposition des composants graphiques
		Container pane = getContentPane();
		pane.setLayout(new FlowLayout());
		pane.add(messageAEnvoyer);
		pane.add(b_Envoyer);
		
		
		
		//Cr�ation du Socket client
		try {
			maSocket = new Socket(adresseServeur, numeroPort);
			writerClient = new PrintWriter(maSocket.getOutputStream()); 
		} catch (Exception e) {
			System.out.println("Erreur Cr�ation client");
		}
		
		try {
			readerClient = new BufferedReader(new InputStreamReader(maSocket.getInputStream()));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		//Affichage de la fen�tre
		setVisible(true);
	}

	/*___________ M�thodes ______________*/
	@Override
	public void actionPerformed(ActionEvent e) {
		//Clic du bouton Envoyer d�clanche la m�thode emettre()
		if (e.getSource() == b_Envoyer) {
			emettre();
		}

	}
	
	private void ecouter() {
		String ligne; 
		try {
		while ((ligne = readerClient.readLine()) != null){ 
			System.out.println(ligne);
		}} catch(IOException e) {
			e.printStackTrace();
			
		}	
	}
	
	

	public void emettre() {
		//Envoi du message
		try {
			String message = messageAEnvoyer.getText(); 
			writerClient.println(message); //Envoi du texte par l'objet writer
			writerClient.flush();
			messageAEnvoyer.setText("");
			messageAEnvoyer.requestFocus();
			} catch (Exception e) {
			System.out.println("Erreur Envoi du Message");
		}
	}

	/*___________ M�thode Main ______________*/
	public static void main (String[] args){
		new ChatClient();
	}

}
